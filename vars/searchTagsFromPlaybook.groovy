import jobrunner.Common
def call(args = [:], Common common = null) {
    args = [
            playbookFile : '',
            getFromFile : true,
            tags: '',
            inventoryPath: '',
            limits: null,
            extras: [],
            vault_args : [:],
            srcGroupVarsPath : '',
            dstGroupVarsPath : '',
            groupVarsFile : '',
    ] << args

    ArrayList result = []

    def playbookContent
    def playbooks
    def spec = [
            inventory: args.inventoryPath,
            limits    : (args?.limits) ? " -l ${args.limits}" : '',
            extras   : (args?.extras) ? " ${args.extras.join(' ')}" : '',
            tags: (args?.tags?.contains('-t')) ? " ${args.tags}" : " -t ${args.tags}"
    ]

    if (args.tags == '' || spec.extras.contains("--skip-tags") || args.extras.find{ it =~ /^\s*-t\s*/}){
        spec.tags = ''
    }

    println "Get all tags from current play"
    // Инициализируем переменные
    if (!common) {
        common = new Common(this)
    }

    if (args.getFromFile) {
        playbookContent = readFile(args.playbookFile).split('\n')
        playbooks = common.matchInVars(playbookContent, '\\S+\\.yml')
    } else {
        playbooks = [args.playbookFile]
    }

    for (String playbook : playbooks) {
        vault_vars_extra = [:]
        if (args.vault_args.size() > 0) {
            vault_vars_extra = common.ansibleCreateVaultFile(args.vault_args, '', "${args.srcGroupVarsPath}", "${args.dstGroupVarsPath}", "${args.groupVarsFile}")
        }
        str_vault = ''
        if (vault_vars_extra?.vaultFile != '' && vault_vars_extra?.passwordFile) {
            str_vault = " --extra-vars @${vault_vars_extra.vaultFile} --vault-password-file ${vault_vars_extra.passwordFile}"
        }
        if (vault_vars_extra?.vaultFile == '' && vault_vars_extra?.passwordFile) {
            str_vault = " --vault-password-file ${vault_vars_extra.passwordFile}"
        }

        String script = """
    ansible-playbook ${playbook} -i ${spec.inventory}${spec.limits}${spec.tags}${spec.extras}${str_vault} --list-tags  2>&1 |
    grep "TASK TAGS" |
    cut -d":" -f2 |
    awk '{sub(/\\[/, "")sub(/\\]/, "")}1' |
    sed -e 's/,//g' |
    xargs -n 1 |
    sort -u
    """
        result.addAll(sh(script:script, returnStdout: true).split('\n'))
    }

    return result
}
