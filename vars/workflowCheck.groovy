#!/usr/bin/groovy
import groovy.json.JsonOutput
import workflower.Helper
import workflower.AnsiblePlaybokContent

def call(args = [:]) {

    //Set Default Args
    args = [
            node               : 'master',
            debugVerbosity     : true,
            zones              : [:],
            schemaPath         : "workflower/Helper/workflowCDSchemaTemplate.json",
            inventory_repo     : "",
            branch             : "master",
            credentialsId      : null,
            ansibleRolesProject: '/ansible/ansible-downloader.git',
            ansibleRolesBranch : 'master',
            ansibleRolesSubdir : 'ansible_deploy',
            playbook           : "playbook.yml",
    ] << args

    assert args.zones != null && args.zones != []

    def defenition
    def currentDir
    def workflow
    def runs = [:]
    def zones = args.zones.collect{ it.key }
    Helper helper = new Helper(this)

    node(args.node) {
        currentDir = pwd()
        deleteDir()
        stage("Checkout projects") {
            helper.gitDefaultCheckout(args.inventory_repo, args.branch, "inventory", args.credentialsId)
            workflow = readFile("${currentDir}/inventory/workflow.json")
            // Для превью показана возможность парсинга плэйбука из гит репозитория.
            // можно заменить на любой другой источник данных
            helper.gitDefaultCheckout(args.ansibleRolesProject, args.ansibleRolesBranch, args.ansibleRolesSubdir, args.credentialId, true, false)

        }
        stage('Check workflow defenition') {
            def schemaObject = helper.getStringSchemaObjectFromResource(args.schemaPath, true, [zones: helper.jsonDump(zones), zones_size: zones.size()])

            if (args.debugVerbosity) {
                println "Current schema:\n${schemaObject}"
            }

            defenition = helper.validateCDWorkflow(workflow, args.schemaPath, true, [zones: helper.jsonDump(zones), zones_size: zones.size()])
            def currentZones = []

            //println "${helper.jsonDump(defenition)}"

            defenition.stages.each { s ->
                if (zones.contains(s.zone) && !currentZones.contains(s.zone)) {
                    currentZones << s.zone
                }
            }
            println "Required zones: ${zones.sort()}"
            println "Current zones from stages: ${currentZones.sort()}"
            //Необходимо для того, чтобы удостовериться, что workflow покрывает все зоны
            assert currentZones == zones
        }
        stage("Generate hosts from patterns") {
            for (s in defenition.stages) {
                def zoneRuns = []
                for (step in s.steps) {
                    for (build in step.builds) {
                        pattern = build.limits.include.join(",")
                        if (build.limits.exclude && build.limits.exclude.size() > 0) {
                            def exclude_limits = build.limits.exclude.join(",!")
                            if (pattern != '') {
                                pattern += ",!${exclude_limits}"
                            } else {
                                pattern = "!${exclude_limits}"
                            }
                        }
                        if (!build["tags"]) {
                            build["tags"] = ["include": ["all"]]
                        }

                        if (!build["verbose"]) {
                            build["verbose"] = "v"
                        }

                        if (!build["forks"]) {
                            build["forks"] = 5
                        }

                        if (!build["serial"]) {
                            build["serial"] = "'100%'"
                        }
                        
                        zoneRuns << ["pattern": pattern, "tags": build["tags"]]
                    }
                }
                runs[s.zone] = helper.jsonParse(helper.getHostsFromStruct("${currentDir}/inventory/${s.zone}", zoneRuns))
            }

            println "Runs:\n${helper.jsonDump(runs)}"
        }


        stage("Generate playbook struct") {

            for (env in runs) {
                println "Get plays from zone: ${env.key}..."
                AnsiblePlaybokContent plays = new AnsiblePlaybokContent(this, "${currentDir}/${args.ansibleRolesSubdir}", args.playbook, "${currentDir}/inventory/${env.key}")
                plays.init()
                for (run in env.value["runs"]) {
                    def bufHosts = [:]
                    for (host in run["hosts"]) {
                        bufHosts << [(host): plays.getRolesSturct().findAll { p -> p["hosts"].contains(host) }.collect { i ->
                            i.findAll {
                                it.key != 'hosts'
                            }
                        }]
                    }
                    run["hosts"] = bufHosts
                }
            }

            println "Runs with plays:\n${helper.jsonDump(runs)}"

        }


        stage("Check host coverage") {
            runs.each { zone, data ->
                println "Check zone '${zone}'..."
                def allHosts = data["all_hosts"].collect()
                def zoneRuns = helper.deepcopy(data["runs"])
                println "Check invetory coverage..."
                if (zoneRuns.size() > 1) {
                    def resHosts = []
                    zoneRuns.each { r ->
                        resHosts.addAll(r["hosts"].keySet())
                    }

                    if (allHosts.sort() != resHosts.sort()) {
                        if (allHosts.size() > resHosts.size()) {
                            def res = (allHosts - resHosts).join()
                            runs[zone]["error"] = "Не найден iventory_hostname: '${res}'"
                        }
                        if (resHosts.size() > allHosts.size()) {
                            def unique = resHosts.findAll { a -> resHosts.findAll { b -> b == a }.size() == 1 }
                            resHosts.removeAll(unique)
                            def res = resHosts.unique().join()
                            runs[zone]["error"] = "В структуре имеются дубликаты inventory_hostname: '${res}'"
                        }
                        if (resHosts.size() == allHosts.size()) {
                            def res = (allHosts - resHosts).join(",")
                            if (res == '') {
                                def dublicates = resHosts.unique().join(",")
                                runs[zone]["error"] = "В структуре имеются дубликаты inventory_hostname: '${dublicates}'"
                            } else {
                                def unique = resHosts.findAll { a -> resHosts.findAll { b -> b == a }.size() == 1 }
                                resHosts.removeAll(unique)
                                def dublicates = resHosts.unique().join(",")
                                runs[zone]["error"] = "В структуре имеются дубликаты inventory_hostname: '${dublicates}'"
                                runs[zone]["error"] += "\nA также отсутствуют inventory_hostname: '${res}'"
                            }

                        }
                    }
                } else {
                    if (allHosts.sort() != zoneRuns[0]["hosts"].sort()) {
                        def res = (allHosts - zoneRuns[0]['hosts']).join(",")
                        runs[zone]["error"] = "Не найден iventory_hostname: '${res}'"
                    }
                }

                println "Check plays coverage..."
                def error = ''
                for (run in data.runs) {
                    for (host in run["hosts"]) {
                        def run_plays = host.value.findAll { p -> p.tags.size() == p.tags.collect { t -> run["tags"]["include"].contains("all") || t == "always" || run["tags"]["include"].contains(t) }.size() }
                        def skip_plays = []
                        if (run["tags"]["exclude"]) {
                            skip_plays = host.value.findAll { p -> p.tags.size() == p.tags.collect { t -> run["tags"]["exclude"].contains(t) }.size() }
                        }
                        run_plays = run_plays - skip_plays
//                            println "run_plays:\n ${helper.jsonDump(run_plays)}"
//                            println "skip_plays:\n ${helper.jsonDump(skip_plays)}"

                        if (run_plays.size() != host.value.size()) {
                            def tagsInfo = helper.jsonDump(run["tags"])
                            def notRunningPlays = helper.jsonDump(host.value - run_plays)
                            error += "По iventory_hostname: '${host.key}', запущенному с тэгами:\n${tagsInfo}\nОтсуствует покрытие плэев:\n${notRunningPlays}\n"
                        }

                    }
                }
                if (error != '') {
                    runs[zone]["error"] = error
                }
            }

            if (runs.find { z, d -> d.keySet().contains("error") }) {
                helper.abortPipeline("Найдены ошибки при проверке структуры лимитов:\n${helper.jsonDump(runs)}", false)
            }
        }

    }

    return defenition
}