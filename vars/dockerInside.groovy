def call(args = [:], Closure body = null) {
    args = [
            dockerInside: false,
            image       : '',
            bindMounts: []
    ] << args

    if (args.dockerInside == true) {
        def mount_args = ''
        args.bindMounts.each{
            mount_args += " -v ${it}:${it}"
        }
        docker.image(args.image).inside(mount_args.trim()) {
            body.call()
        }
    } else {
        body.call()
    }
}
