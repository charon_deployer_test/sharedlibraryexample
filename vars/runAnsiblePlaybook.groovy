import jobrunner.Common

def call(args = [:], Common common = null) {
    args = [
            ansibleEnv: [],
            playbookFile : '',
            vault_args : [:],
            srcGroupVarsPath : '',
            dstGroupVarsPath : '',
            groupVarsFile : '',
            ansibleVerbose : 'v',
            tags : '',
            become : true,
            limits : '',
            forks : "5",
            extras : [],
            stageName : "Run ansible Playbook",
            getFromFile : true,
            inventoryPath : '',
            docker: [
                    dockerInside : false,
                    image       : '',
                    bindMounts: []
            ]

    ]<<args

    if (!common) {
        common = new Common(this)
    }

    def playbookContent
    def playbooks


    stage(args.stageName) {


        if (args.getFromFile) {
            playbookContent = readFile(args.playbookFile).split('\n')
            playbooks = common.matchInVars(playbookContent, '\\S+\\.yml')
        } else {
            playbooks = [args.playbookFile]
        }

        dockerInside( args.docker ){

            for (String playbook : playbooks) {
                vault_vars_extra = [:]
                if (args.vault_args.size() > 0) {
                    vault_vars_extra = common.ansibleCreateVaultFile(args.vault_args, '', "${args.srcGroupVarsPath}", "${args.dstGroupVarsPath}", "${args.groupVarsFile}")
                }
                def playbookName = playbook.trim()

                withEnv(args.ansibleEnv) {
                    common.ansibleRunSinglePlaybook(
                            verbose: args.ansibleVerbose,
                            forks: args.forks.toInteger(),
                            tags: args.tags,
                            vault: vault_vars_extra,
                            extrasKeys: args.extras,
                            limit: args.limits,
                            become: args.become,
                            playbookName,
                            "../${args.inventoryPath}"
                    )
                }
            }
        }
    }
}
