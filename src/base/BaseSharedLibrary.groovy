package base

import groovy.json.JsonOutput
import groovy.json.StringEscapeUtils
import hudson.plugins.ansicolor.SimpleHtmlNote
import com.cloudbees.groovy.cps.NonCPS
import groovy.text.GStringTemplateEngine
import org.jenkinsci.plugins.pipeline.utility.steps.shaded.org.yaml.snakeyaml.Yaml

class BaseSharedLibrary implements Serializable {
    protected final script

    /**
     * This constructor sets the Jenkins 'script' class as the local script
     * variable in order to resolve execution steps.(sh, withCredentials, etc)
     * @param script the script object
     */
    BaseSharedLibrary(script) {
        this.script = script
    }

    def parseYaml(filepath){
        Yaml parser = new Yaml()
        def res = parser.load((filepath as File).text)
        return res
    }

    /**
     * serrialazable parsing json
     * @param json string
     * @return @Map object
     */
    @NonCPS
    def jsonParse(String json) {
        return new groovy.json.JsonSlurperClassic().parseText(json)
    }

    /**StringEscapeUtils
     * serrialazable json dump
     * @param data
     * @return @String prettyPrint unescaped JSON
     */
    @NonCPS
    String jsonDump(def data) {
        def json = new groovy.json.JsonBuilder(data).toString()
        json = StringEscapeUtils.unescapeJavaScript(JsonOutput.prettyPrint(json))
        return json
    }

    /**
     * Generate HTML <span> colorize tag and insert text to it
     * @param color - color usege from colorTags @map
     * @param fontWeight - usage one of two variants: normal or bold
     * @return @ConsoleNote encoded String object.
     */
    @NonCPS
    String colorTextGenerator(String text, String color = 'red', String fontWeight = 'normal') {
        return new SimpleHtmlNote($/<span style="color: ${color}; font-weight=${fontWeight}">${
            text
        }</span> /$).encode().toString()
    }

    //https://stackoverflow.com/questions/13155127/deep-copy-map-in-groovy
    /**
     * Функция предназначена для копирования объектов
     * @param orig
     * @return
     */
    def deepcopy(orig) {
        def bos = new ByteArrayOutputStream()
        def oos = new ObjectOutputStream(bos)
        oos.writeObject(orig); oos.flush()
        def bin = new ByteArrayInputStream(bos.toByteArray())
        def ois = new ObjectInputStream(bin)
        return ois.readObject()
    }

    /**
     * Clone git repo.
     * @param args - optional parameters (map):
     * subdir - target dir to checkout to;
     * clean_before - clean target dir before checkout;
     * clean_after - clean target dir after checkout;
     * wipeWorkspace - force clean target dir before checkout.
     * @param url full url to git project.
     * @param branchOrTag branch or tag for checkout.
     * @param credentialsId jenkins's id of credentials object.
     * @return void* */
    def gitCheckout(Map args = [:], url, branchOrTag, credentialsId) {
        def _args = [subdir: '', cleanBefore: false, cleanAfter: false, wipeWorkspace: false, useTags: false] << args
        String tags = (args.useTags) ? 'tags' : 'heads'
        String remoteTags = (tags) ? 'tags/' : ''
        def refspec = "+refs/${tags}/${branchOrTag}:refs/remotes/origin/${remoteTags}${branchOrTag}"
        def spec = [
                $class                           : 'GitSCM',
                branches                         : [
                        [
                                name: branchOrTag
                        ]
                ],
                doGenerateSubmoduleConfigurations: false,
                extensions                       : [],
                submoduleCfg                     : [],
                userRemoteConfigs                : [
                        [
                                credentialsId: credentialsId,
                                refspec      : refspec,
                                url          : url
                        ]
                ]
        ]
        if (_args.subdir) {
            spec.extensions << [$class: 'RelativeTargetDirectory', relativeTargetDir: _args.subdir]
        }
        if (_args.cleanBefore) {
            spec.extensions << [$class: 'CleanBeforeCheckout']
        }
        if (_args.cleanAfter) {
            spec.extensions << [$class: 'CleanCheckout']
        }
        if (_args.wipeWorkspace) {
            spec.extensions << [$class: 'WipeWorkspace']
        }
        return this.script.checkout(spec)
    }

    /**
     * Checkout project from default gitlab server.
     * @param pathProject relative path of project in URL.
     * @param branchOrTag to clone
     * @param subdir path to directory, where project will be cloned
     * @return void* */
    def gitDefaultCheckout(String fullUrl, String branchOrTag, String subdir, String credentialsId = null, wipeWorkspace = true, useTags = false) {
        // Probably it's better to put following values to parameters or prop file, or somewhere ...
        return gitCheckout(subdir: subdir, cleanBefore: true, cleanAfter: true, wipeWorkspace: wipeWorkspace, useTags: useTags, fullUrl, branchOrTag, credentialsId)
    }


    /**
     * Функция предназначена для вызова исключения и завершения текущего пайплайна.
     * @param msg
     * @param colorize
     * @param fail
     */
    void abortPipeline(String msg = '', Boolean colorize = true, Boolean fail = true) {
        if (colorize == true){
            msg = colorTextGenerator(msg)
        }
        this.script.currentBuild.result = (fail) ? 'FAILURE' : 'ABORTED'
        throw new hudson.AbortException(msg)
    }

}
